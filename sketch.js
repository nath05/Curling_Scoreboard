// interface graphique avec P5
// Nathan Poirier
// 29-12-2018

// tableau
var top_tab = 350;
var bottom = 450;
var left = 380;
var right = 1385;
var l_name = 180;
var l_bouts = 75;
var n_bouts = 11;
var button_size = 25;
var img;
var mart_state = 0;
// scores
var score_loc = [];
var score_vis = [];
// boutons
var bout_plusXJ = [];
var bout_plusYJ = bottom+button_size;
var bout_moinsXJ = [];
var bout_moinsYJ = bottom+button_size;
var bout_plusXR = [];
var bout_plusYR = bottom;
var bout_moinsXR = [];
var bout_moinsYR = bottom;
var bout_zeroX = [];
var bout_zeroY = bottom;
var bout_XX = [];
var bout_XY = bottom+button_size;
var bout_clearX = [];
var bout_clearY = bottom+2*button_size;
var martX = left+l_name;
var martY = top_tab + (bottom-top_tab)/2;
// scores et totaux
var tot_loc = 0;
var tot_vis = 0;
for (var i = 0; i < n_bouts; i++) {
  score_loc[i] = '';
  score_vis[i] = '';
}
// initialisation boutons
for (var i = 0; i < n_bouts; i++) {
  bout_plusXJ[i] = left+l_name+l_bouts*i;
  bout_plusXR[i] = left+l_name+l_bouts*i;
  bout_moinsXJ[i] = left+l_name+l_bouts*i+button_size;
  bout_moinsXR[i] = left+l_name+l_bouts*i+button_size;
  bout_zeroX[i] = left+l_name+l_bouts*i+2*button_size;
  bout_XX[i] = left+l_name+l_bouts*i+2*button_size;
  bout_clearX[i] = left+l_name+l_bouts*i;
}

function preload(){
  img = loadImage("assets/marteau.png");
}

function setup(){
  createCanvas(1820,880);
}

function tableau(){
  strokeWeight(2);
  rect(left,top_tab,right-left,(top_tab-bottom)/2);
  fill(255,0,0);
  rect(left,bottom+(top_tab-bottom)/2,right-left + 110,(top_tab-bottom)/2);
  fill(255,255,0);
  rect(left,bottom,right-left + 110,(top_tab-bottom)/2);
  // ligne du top
  line(left,top_tab,right+110,top_tab);
  // ligne du bas
  line(left,bottom,right+110,bottom);
  //ligne du milieu
  line(left,bottom + (top_tab-bottom)/2,right+110,bottom + (top_tab-bottom)/2);
  // ligne Total
  line(left + l_name + (l_bouts*n_bouts)+110,top_tab,left + l_name + (l_bouts*n_bouts)+110,bottom);
  // ligne left
  line(left,top_tab,left,bottom);
    //ligne pour les noms des equipes
  line(left + l_name,top_tab,left + l_name,bottom);
  // lignes pour les bouts
  for (var i = 1; i <= n_bouts; i++) {
    line(left + l_name + (l_bouts*i),top_tab,left + l_name + (l_bouts*i),bottom);
  }
  line(left+l_name,bottom+2*button_size,left + l_name + (l_bouts*n_bouts),bottom+2*button_size);
  line(left+l_name,bottom+button_size,left + l_name + (l_bouts*n_bouts),bottom+button_size);
}

function draw_text(){
  textSize(36);
  fill(50);
  text('Club de Curling de Baie-Comeau',left + 300,top_tab-100);
  textSize(32);
  text('Rouge',left + 35,bottom+(top_tab-bottom)*4/6);
  text('Jaune',left + 35,bottom+(top_tab-bottom)*1/6);
  fill(255,255,255);
  text('Bouts',left + 50,top_tab - 15);
  fill(50);
  text('Total',left + l_name + (l_bouts*n_bouts)+ 20,top_tab - 15);
  text(tot_loc.toString(),left + l_name + (l_bouts*n_bouts)+ 45,bottom+(top_tab-bottom)*4.5/7);
  text(tot_vis.toString(),left + l_name + (l_bouts*n_bouts)+ 45,bottom+(top_tab-bottom)*1/7);
  // totaux
  //text(tot_loc.toString(),right+90,top_tab+20);
  //text(tot_vis.toString(),right+90,(top_tab-bottom)/2 - 20);
  for (var i = 0; i < n_bouts; i++) {
    // scores
    text(score_loc[i].toString(),left + l_name + (l_bouts*i) + l_bouts*2/5,bottom+(top_tab-bottom)*4.5/7);
    text(score_vis[i].toString(),left + l_name + (l_bouts*i) + l_bouts*2/5,bottom+(top_tab-bottom)*1/7);
    n = i+1;
    if (n < 9) {
      // numeros de Bouts
      fill(255,255,255);
      text(n.toString(),left + l_name + (l_bouts*i) + l_bouts*2/5,top_tab-15);
      fill(50);
    }
    else {
      fill(255,255,255);
      text(n.toString(),left + l_name + (l_bouts*i) + l_bouts*1/4,top_tab-15);
      fill(50);
    }
  }
}

function buttons(){
  // http://coursescript.com/notes/interactivecomputing/interactivity/
  // affichage des boutons
  for (var i = 0; i < n_bouts; i++) {
    fill(255,255,0);
    rect(bout_plusXJ[i],bout_plusYJ,button_size,button_size);
    rect(bout_moinsXJ[i],bout_moinsYJ,button_size,button_size);
    fill(255,0,0);
    rect(bout_plusXR[i],bout_plusYR,button_size,button_size);
    rect(bout_moinsXR[i],bout_moinsYR,button_size,button_size);
    fill(50);
    rect(bout_clearX[i],bout_clearY,button_size*3,button_size);
    textSize(24);
    text('+',bout_plusXJ[i]+button_size*1/4,bout_plusYJ+button_size*3/4);
    text('-',bout_moinsXJ[i]+button_size*1/3,bout_moinsYJ+button_size*3/4);
    textSize(18);
    text('0',bout_zeroX[i]+button_size*1/3,bout_zeroY+button_size*3/4);
    textSize(24);
    text('+',bout_plusXR[i]+button_size*1/4,bout_plusYR+button_size*3/4);
    text('-',bout_moinsXR[i]+button_size*1/3,bout_moinsYR+button_size*3/4);
    textSize(16);
    text('X',bout_XX[i]+button_size*1/3,bout_XY+button_size*3/4);
    fill(255,255,255);
    text('Effacer',bout_clearX[i]+15,bout_clearY+button_size*2/3);
    fill(50);
    textSize(50);
  }
  textSize(24);
  rect(left + l_name + (l_bouts*n_bouts),bottom,110,button_size*2.0);
  fill(255,255,255);
  text('Eff. tout',left + l_name + (l_bouts*n_bouts)+ 10,bottom + 35);
  fill(50);
  if (mouseX >= left+l_name && mouseX <= left + l_name + (l_bouts*n_bouts)+110 && mouseY >= bottom && mouseY <= bottom+button_size*3.0) {
    cursor(HAND);
  }
  else if (mouseX >= left && mouseX <= martX && mouseY >= top_tab && mouseY <= bottom) {
    cursor(HAND);
  }
  else {
    cursor(ARROW);
  }
}

function mousePressed(){
  score();
}

function score(){
  tot_loc = 0;
  tot_vis = 0;
  for (var i = 0; i < n_bouts; i++) {
    if (mouseX >=bout_plusXJ[i] && mouseX <= bout_plusXJ[i]+button_size && mouseY >= bout_plusYR && mouseY <= bout_plusYR+button_size) {
      if (score_loc[i] == '' || score_loc[i] == 'X') {
        score_loc[i] = 0;
        score_vis[i] = 0;
      }
      score_loc[i] += 1;
    }
    if (mouseX >=bout_moinsXJ[i] && mouseX <= bout_moinsXJ[i]+button_size && mouseY >= bout_moinsYR && mouseY <= bout_moinsYR+button_size) {
      if (score_loc[i] == '' || score_loc[i] == 'X') {
        score_loc[i] = 0;
        score_vis[i] = 0;
      }
      score_loc[i] -= 1;
    }
    if (mouseX >=bout_plusXR[i] && mouseX <= bout_plusXR[i]+button_size && mouseY >= bout_plusYJ && mouseY <= bout_plusYJ+button_size) {
      if (score_vis[i] == '' || score_vis[i] == 'X') {
        score_vis[i] = 0;
        score_loc[i] = 0;
      }
      score_vis[i] += 1;
    }
    if (mouseX >=bout_moinsXR[i] && mouseX <= bout_moinsXR[i]+button_size && mouseY >= bout_moinsYJ && mouseY <= bout_moinsYJ+button_size) {
      if (score_vis[i] == '' || score_vis[i] == 'X') {
        score_loc[i] = 0;
        score_vis[i] = 0;
      }
      score_vis[i] -= 1;
    }
    if (mouseX >=bout_zeroX[i] && mouseX <= bout_zeroX[i]+button_size && mouseY >= bout_zeroY && mouseY <= bout_zeroY+button_size) {
      score_loc[i] = 0;
      score_vis[i] = 0;
    }
    if (mouseX >=bout_XX[i] && mouseX <= bout_XX[i]+button_size && mouseY >= bout_XY && mouseY <= bout_XY+button_size) {
      score_loc[i] = 'X';
      score_vis[i] = 'X';
    }
    if (mouseX >= bout_clearX[i]&&mouseX <= bout_clearX[i]+button_size*3.0 && mouseY >= bout_clearY && mouseY<= bout_clearY + button_size) {
      score_loc[i] = '';
      score_vis[i] = '';
    }
    if (mouseX >= left && mouseX <= martX && mouseY >= top_tab && mouseY <= martY) {
      mart_state = 0;
    }
    if (mouseX >= left && mouseX <= martX && mouseY >= martY && mouseY <= bottom) {
      mart_state = 1;
    }
    if (score_loc[i] < 0) {
      score_loc[i] = 0;
    }
    if (score_vis[i] < 0) {
      score_vis[i] = 0;
    }
    if (score_loc[i] > 8) {
      score_loc[i] = 8;
    }
    if (score_vis[i] > 8) {
      score_vis[i] = 8;
    }
  }
  if (mouseX >= left + l_name + (l_bouts*n_bouts) && mouseX <= left + l_name + (l_bouts*n_bouts)+ 110 && mouseY >= bottom && mouseY <= bottom + button_size*2.0) {
    for (var i = 0; i < n_bouts; i++) {
      score_loc[i] = '';
      score_vis[i] = '';
    }
  }
  for (var i = 0; i < n_bouts; i++) {
    if (score_loc[i] != '' && score_loc[i] != 'X') {
      tot_loc += score_loc[i];
    }
    if (score_vis[i] != '' && score_vis[i] != 'X') {
      tot_vis += score_vis[i];
    }
  }
}

function draw(){
  clear();
  tableau();
  draw_text();
  buttons();
  if (mart_state == 0) {
    image(img,left+l_name - 30,top_tab+12,20,20);
  }
  if (mart_state == 1) {
    image(img,left+l_name - 30,bottom -36,20,20);
  }
}
